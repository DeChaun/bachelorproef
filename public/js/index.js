// --- listeners --- //

const initListeners = () => {
    const filterButton = document.getElementById('filter-button');
    const interestsShowMore = document.getElementById('filter-interests-show-more');
    const loginButton = document.getElementById('login-button');
    const logoutButton = document.getElementById('logout-button');
    
    filterButton.addEventListener('click', handleFilterClick);
    interestsShowMore.addEventListener('click', handleInterestShowMore);
    loginButton.addEventListener('click', handleLoginButtonClick);
    logoutButton.addEventListener('click', handleLogoutButtonClick);
}




// --- ophalen resultaten --- //

const handleFilterClick = () => {
    // ophalen geselecteerde values
    const filterYear = document.getElementById('filter-birthyear');
    const filterCountry = document.getElementById('filter-country');
    const filterInterests = document.querySelectorAll('.filter-interests>input:checked');

    let filterNameValue = document.getElementById('filter-lname').value;
    let filterYearValue = filterYear.options[filterYear.selectedIndex].value;
    let filterCountryValue = filterCountry.options[filterCountry.selectedIndex].value;
    let filterInterestsValues = [];

    filterInterests.forEach(i => { filterInterestsValues.push(i.value) });

    // ophalen data van API
    getFilteredDataFromGraphQLAPI({
        "name": filterNameValue,
        "year": filterYearValue,
        "country": filterCountryValue,
        "interests": filterInterestsValues
    }, (err, res) => { err ? logError(err) : showUserData(res) });
}

const handleInterestShowMore = () => {
    const filterInterests = document.getElementsByClassName('filter-interests');
    const interestsShowMore = document.getElementById('filter-interests-show-more');

    for (let interest of filterInterests) {
        interest.classList.remove('hidden')
    }

    interestsShowMore.classList.add('hidden');
}

const handleLoginButtonClick = () => {
    login((err, isAuthOk) => {
        console.log(err, isAuthOk);
        if (isAuthOk) {
            document.getElementsByClassName('message-login-true')[0].classList.remove('hidden');
            document.getElementsByClassName('message-login-false')[0].classList.add('hidden');
        }
    });
}

const handleLogoutButtonClick = () => {
    logout((err, isAuthOk) => {
        console.log(err, isAuthOk);
        
        document.getElementsByClassName('message-login-false')[0].classList.remove('hidden');
        document.getElementsByClassName('message-login-true')[0].classList.add('hidden');
    });
}

const showUserData = (users) => {
    console.log('users', users);
    // tabel leegmaken & weergeven van users in tabel
    let tableBody = document.querySelector('#table-result>tbody');
    tableBody.innerHTML = "";

    users.forEach(u => {

        let tr = document.createElement("tr");

        let tdName = document.createElement("td");
        let tdBday = document.createElement("td");
        let tdCity = document.createElement("td");
        let tdAddress = document.createElement("td");
        let tdCountry = document.createElement("td");
        let tdInterests = document.createElement("td");

        tdName.textContent = u.firstname + " " + u.lastname;
        tdBday.textContent = formatDate(u.birthday);
        tdAddress.textContent = u.address;
        tdCity.textContent = u.city;
        tdCountry.textContent = u.country;
        
        // toevoegen interestlijst
        let tdInterestsUl = document.createElement("ul");
        u.interests.forEach(i => {
            let li = document.createElement("li");
            li.textContent = i;
            tdInterestsUl.appendChild(li);
        });

        tdInterests.appendChild(tdInterestsUl);

        tr.appendChild(tdName);
        tr.appendChild(tdBday);
        //tr.appendChild(tdAddress);
        tr.appendChild(tdCity);
        tr.appendChild(tdCountry);
        tr.appendChild(tdInterests);

        tableBody.appendChild(tr);

    });
}




// --- ophalen data voor selecteren filters --- //

const showBirthYears = (years) => {
    const yearFitlerSelect = document.getElementById('filter-birthyear');

    years.sort((a, b) => { return b - a; });

    years.forEach(year => {
        let option = document.createElement('option');
        option.value = year;
        option.textContent = year;
        yearFitlerSelect.appendChild(option);
    });
}

const showCountries = (countries) => {
    const countryFilterSelect = document.getElementById('filter-country');

    countries.sort();

    countries.forEach(country => {
        let option = document.createElement('option');
        option.value = country;
        option.textContent = country;
        countryFilterSelect.appendChild(option);
    });
}

const showInterests = (interests) => {
    const interestsFilterContainer = document.getElementById('filter-interests-container');

    interests.sort();

    for (let i = 0; i < interests.length; i++) {
        let interest = interests[i];
        
        let filterInterest = document.createElement('section');
        filterInterest.classList.add('filter-interests');

        if (i > 20) filterInterest.classList.add('hidden');

        let filterInterestInputCheck = `<input type="checkbox" id="filter-interests-${ interest }" name="filter-interests-selection" value="${ interest }">`;
        let filterInterestLabel = `<label for="filter-interests-${ interest }" >${ interest }</label>`;

        filterInterest.innerHTML += filterInterestInputCheck;
        filterInterest.innerHTML += filterInterestLabel;

        interestsFilterContainer.appendChild(filterInterest);
    };
}




// --- initialiseer --- //
(() => {

    getYearsFromGraphQLAPI((err, res) => { err ? logError(err) : showBirthYears(res) });
    getCountriesFromGraphQLAPI((err, res) => { err ? logError(err) : showCountries(res) });
    getInterestsFromGraphQLAPI((err, res) => { err ? logError(err) : showInterests(res) });

    initListeners();
    
})();




// --- hulpmethodes --- //

const formatDate = (date) => {
    let pieces = date.split('-');
    return pieces[2] + "-" + pieces[1] + "-" + pieces[0];
}




// --- errorlogging --- //

// const logError = (err) => {
//     console.log(err);
//     const errorDiv = document.querySelector('.message-errors div:last-child');
//     errorDiv.textContent = err;
// }