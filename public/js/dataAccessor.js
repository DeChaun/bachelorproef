const getUsersFromGraphQLAPI = (callback) => {
    let query = {};
    sendGraphQLRequest('/api/users', query, null, (res) => { callback(null, res) });
}

const getYearsFromGraphQLAPI = (callback) => {
    let query = `{ birthyears }`;
    sendGraphQLRequest('/api/users', query, null, (res) => { callback(null, res.birthyears) });
}

const getCountriesFromGraphQLAPI = (callback) => {
    let query = `{ countries }`;
    sendGraphQLRequest('/api/users', query, null, (res) => { callback(null, res.countries) });
}

const getInterestsFromGraphQLAPI = (callback) => {
    let query = `{ interests }`;
    sendGraphQLRequest('/api/users', query, null, (res) => { callback(null, res.interests) });
}

const getFilteredDataFromGraphQLAPI = (data, callback) => {
    let query = `query(
        $country : String,
        $lastname : String,
        $interests : [String],
        $birthyear: String) {
            users(country: $country, lastname: $lastname, interests: $interests, birthyear: $birthyear) {
                _id,
                firstname,
                lastname,
                interests,
                country,
                birthday,
                city
            }
        }`;

    let args = { country: data.country, lastname: data.name, interests: data.interests, birthyear: data.year };

    sendGraphQLRequest('/api/users', query, args, (res) => { callback(null, res.users) });
}




// --- authenticatie --- //

const login = (callback) => {
    sendAuthRequest('/auth/login', (status) => {
        status === 200 ? callback(null, status === 200) : callback("unauthorized", false);
    });
}

const logout = (callback) => {
    sendAuthRequest('/auth/logout', (status) => {
        status === 200 ? callback(null, status !== 200) : callback("An internal server error occured: Status code " + status, true);
    });
}




// --- XMLHTTP requests --- //

const sendGraphQLRequest = (url, query, args, callback) => {

    var xhttp = new XMLHttpRequest();

    xhttp.responseType = 'json';
    xhttp.open("POST", url);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Accept", "application/json");

    xhttp.onload = function () {
        callback(xhttp.response.data);
    }

    getFromStorage("token", (err, token) => {
        if (token) xhttp.setRequestHeader("Authorization", token);

        xhttp.send(JSON.stringify({
            query: query,
            variables: args,
        }));
    });
    
}

const sendAuthRequest = (url, callback) => {
    let xhttp = new XMLHttpRequest();

    xhttp.open("POST", url, true);
    xhttp.onreadystatechange = function() {
        // readyState 4 = ok
        if (this.readyState == 4) {
            store(JSON.parse(this.response).token, (err, token) => {
                if (!err) {
                    let msg = token ? "Token will be removed" : "Token will be saved";
                    logMessage(msg);
                } else {
                    logError(err);
                }
            });
            callback(this.status);
        }
    };

    getFromStorage("token", (err, token) => {
        if (token) xhttp.setRequestHeader("Authorization", token);

        xhttp.send();
    });
}




// --- benchmarks --- //

const getFilteredDataFromGraphQLAPIBenchmark = (data, callback) => {

    let url = "/api/users";

    sendBenchmarkXMLHttpPost(url, data, (res) => {
        if (data) { logRequestTime(data.size, data.speed); }

        if (res) {
            callback(null, res);
        } else {
            callback("Could not get data", null);
        }
    });

}




const sendBenchmarkXMLHttpPost = (url, data, callback) => {
    let xhttp = new XMLHttpRequest();

    let timeBeforeRequest = new Date();
    
    xhttp.responseType = 'json';
    xhttp.open("POST", url);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Accept", "application/json");

    xhttp.onload = function () {
        if (this.readyState == 4 && this.status == 200) {
            callback({
                "size": this.getResponseHeader('Content-Length'),
                "speed": new Date() - timeBeforeRequest
            });
        }
    }

    getFromStorage("token", (err, token) => {
        if (token) xhttp.setRequestHeader("Authorization", token);

        xhttp.send(JSON.stringify({
            query: data.query,
            variables: data.args,
        }));
    });
}




// --- local storage --- //

const store = (token, callback) => {
    if (typeof(Storage) !== 'undefined') {
        // localstorage wordt ondersteund door browser
        if (!token) {
            // token bestaat niet (meer)
            callback(null, undefined);
            // indien token al in db zat -> verwijder
            if (localStorage.token)
                localStorage.removeItem('token')
        } else {
            localStorage.token = token;
            callback(null, token);
        }
    } else {
        callback("Local storage not supported", null);
    }
}

const getFromStorage = (field, callback) => {
    if (typeof(Storage) !== 'undefined') {
        callback(null, localStorage.token);
    } else {
        callback("Local storage not supported", null);
    }
}
