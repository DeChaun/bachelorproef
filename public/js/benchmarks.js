// aantal requests dat gedaan wordt
const NUMBER_OF_REQUESTS = 100;

/*
 * Loggen van benchmarkdata
 * numberOfItems: aantal items dat per request opgehaald wordt
 */
const startBenchmarksGraphQL = (numberOfItems) => {
    let country = "";
    
    // beslis hoeveel items de response moet bevatten
    switch (numberOfItems) {
        case 1:
            country = "Afghanistan";
            break;
        case 9:
            country = "Argentina";
            break;
        default:
            country = "";
            break;
    }

    let data = {};
    data.args = { country: country };
    data.query = `query($country : String) {
            users(country: $country) {
                _id,
                firstname,
                lastname,
                interests,
                country,
                birthday,
                city
            }
        }`;

    let speedArray = [];

    // hoeveel requests dienen er te gebeuren?
    for (i = 1; i <= NUMBER_OF_REQUESTS; i++) {
        getFilteredDataFromGraphQLAPIBenchmark(data, (err, res) => {
            let c = counter();
            console.log("counter", c);
            
            // hoeveel tijd had de API-request nodig?
            speedArray.push(res.speed);

            // indien alle requests terug zijn
            if (c === NUMBER_OF_REQUESTS) {
                let totalSpeed = 0;
                let averageSpeed = 0;

                // bereken gemiddelde snelheid
                speedArray.forEach(s => { console.log(s); totalSpeed += s });
                averageSpeed = Math.round(totalSpeed / speedArray.length);
                
                // size is overal hetzelfde -> slechts 1x loggen
                console. info("Response size for " + numberOfItems + " items requested: " + res.size);
                console.info("Average speed: " + averageSpeed + "ms");

                counter(0);
            }
        });
    }
}

const counter = (() => {
    let counter = 0;
    return function count(val) { return val !== undefined ? counter = val : ++counter }
})();
