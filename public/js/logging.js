let logListErr = [];
let logListMsg = [];

const logRequestTime = (requestSize, requestTime) => {
    const logs = document.getElementById('message-logs-msg');
    logs.textContent = `Request of ${ requestSize } Bytes was loaded in ${ requestTime } milliseconds`;
}

const logError = (message) => {
    const logsContainer = document.getElementById('message-logs-err');
    let counter = 0;
   
    let lastCount = getErrorCounter();
    let firstCount = lastCount > 10 ? lastCount - 10 : 0;
    let counter = firstCount;

    logListErr.push(message);
    logsContainer.innerHTML = "";

    if (logListErr.length > 10) { logListErr.shift(); }

    logListErr.forEach(log => {
        counter = counter < 9 ? "0" + ++counter : ++counter;

        let div = document.createElement('div');
        div.textContent = counter + " - " + log;

        logsContainer.appendChild(div);
    });
}

const logMessage = (message) => {
    const logsContainer = document.getElementById('message-logs-msg');
    
    let lastCount = getMessageCounter();
    let firstCount = lastCount > 10 ? lastCount - 10 : 0;
    let counter = firstCount;

    logListMsg.push(message);
    logsContainer.innerHTML = "";

    if (logListMsg.length > 10) { logListMsg.shift(); }

    logListMsg.forEach(log => {
        counter = counter < 9 ? "0" + ++counter : ++counter;

        let div = document.createElement('div');
        div.textContent = counter + " - " + log;

        logsContainer.appendChild(div);
    });
}

var getMessageCounter = (function () {
    var messageCounter = 1;
    return function () { return messageCounter++ }
})();

const getErrorCounter = (() => {
    let errorCounter = 1;
    return function () { return errorCounter++ }
})