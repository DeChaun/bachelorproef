"use strict"

module.exports = class User {
    
    constructor(u) {
        this._id = u.id,
        this.id = u.id;
        this.firstname = u.firstname;
        this.lastname = u.lastname;
        this.birthday = u.birthday;
        this.address = u.address;
        this.city = u.city;
        this.country = u.country;
        this.interests = u.interests;
        this.comment = u.comment;
    }

}