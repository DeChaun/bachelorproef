"use strict"

// GraphQL-declaraties
const expressGraphQL = require('express-graphql');
const { GraphQLInputObjectType,
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLList,
    buildSchema,
    GraphQLInt } = require('graphql');

// andere declaraties
const databaseService = require("../services/database_service");
const config = require("../../config");
const User = require("../models/User");

const bodyParser = require("body-parser");
const express = require("express");
const path = require("path");
const app = express();

// JWT
const jwt = require('jsonwebtoken');


module.exports = (() => {

    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(bodyParser.json());




    // --- JSON Web Tokens --- //

    let blackList = [];

    // indien token bestaat en geverifieerd -> user is ingelogd
    const jwtChecker = (req, res, next) => {
        let token = req.headers.authorization;

        // token staat op blacklist
        if (blackList.indexOf(token) !== -1) {
            console.error("User tried an invalid JWT - blacklisted");
            return next();
        }
        
        jwt.verify(token, config.jwtSecretKey, (err, decodedToken) => {
            err ? console.error("JWT could not be verified") : console.log('User logged in');

            return next();
        });   
    };

    // --> authenticatie
    app.post('/auth/login', (req, res) => {
        jwt.sign({ username: "testuser", "email": "testuser@email.com" },
            config.jwtSecretKey,
            { expiresIn: '60m' },
            (err, token) => { res.json({ token })});
    });

    app.post('/auth/logout', (req, res) => {
        let token = req.headers.authorization;

        blackList.push(token);
        res.json({ token: undefined });
    });




    // --- GraphQL --- //

    const User = new GraphQLObjectType({
        name: 'User',
        fields: {
            _id: { type: GraphQLInt },
            id: { type: GraphQLInt },
            firstname: { type: GraphQLString },
            lastname: { type: GraphQLString },
            birthday: { type: GraphQLString },
            address: { type: GraphQLString },
            city: { type: GraphQLString },
            country: { type: GraphQLString },
            interests: { type: GraphQLList(GraphQLString) },
            comment: { type: GraphQLString },
        }
    });

    const UserInput = new GraphQLInputObjectType({
        name: 'UserInput',
        fields: {
            _id: { type: GraphQLInt },
            id: { type: GraphQLInt },
            firstname: { type: GraphQLString },
            lastname: { type: GraphQLString },
            birthday: { type: GraphQLString },
            address: { type: GraphQLString },
            city: { type: GraphQLString },
            country: { type: GraphQLString },
            interests: { type: GraphQLList(GraphQLString) },
            comment: { type: GraphQLString },
        }
    })

    const RootQuery = new GraphQLObjectType({
        name: 'query',
        fields: {
            users: {
                type: GraphQLList(User),
                args: {
                    lastname:  { type: GraphQLString },
                    country:   { type: GraphQLString },
                    interests: { type: GraphQLList(GraphQLString) },
                    birthyear: { type: GraphQLString }
                },
                resolve(_, args) { return databaseService.getUsersFromDb(args) }
            },
            countries: {
                type: GraphQLList(GraphQLString),
                resolve() { return databaseService.getCountriesFromDb() }
            },
            birthyears: {
                type: GraphQLList(GraphQLInt),
                resolve() { return databaseService.getBirthYearsFromDb() }
            },
            interests: {
                type: GraphQLList(GraphQLString),
                resolve() { return databaseService.getInterestsFromDb() }
            }
        }
    });

    const mutation = new GraphQLObjectType({
        name: 'mutation',
        fields: {
            createusers: {
                type: GraphQLString,
                args: {
                    users: { type: GraphQLList(UserInput) }
                },
                resolve(_, args) { return databaseService.createUser(args.users) }
            },
            removeuser: {
                type: GraphQLString,
                args: {
                    userid: { type: GraphQLString }
                },
                resolve(_, args) { return databaseService.removeUser(args.userid) }
            }
        }
    });

    const schema = new GraphQLSchema({
        mutation: mutation,
        query: RootQuery
    });

    // middleware
    app.use('/api/users', jwtChecker, expressGraphQL({
        schema: schema,
        graphiql: true
    }));

    

    
    // --- Default --- //

    // --> default
	app.get('/', jwtChecker, (req, res) => {
		res.sendFile(path.join(config.mainDir, "/public/index.html"));
    });

    // --> other pages
    app.get('/:fileName', (req, res) => {
		res.sendFile(path.join(config.mainDir, "/public/pages/", req.params.fileName));
    });
    

    // --> js & css & assets
    app.get('/js/:fileName', (req, res) => {
        res.sendFile(path.join(config.mainDir, "/public/js/", req.params.fileName));
    });

    app.get('/css/:fileName', (req, res) => {
        res.sendFile(path.join(config.mainDir, "/public/css/", req.params.fileName));
    });

    app.get('/assets/:fileName', (req, res) => {
        res.sendFile(path.join(config.mainDir, "/public/assets/", req.params.fileName));
    });
    
    


    // -- starting server --- //
    
    app.get('/:fileName', (req, res) => {
		res.sendFile(path.join(config.mainDir, "/public/pages/", req.params.fileName));
    });
	
	app.listen(process.env.PORT || config.port, () => {
		console.log('Listening on port ' + process.env.PORT || config.port);
	});
})();
