const database = require("../database/database");
const User     = require("../database_schemas/schema_user");

exports.getUsersFromDb = (query) => {
    console.log(query, "query");
    return new Promise((resolve, reject) => {
        if (query)
            database.getFilteredUsers(query, (err, users) => { err ? reject(err) : resolve(users) });
        else
            database.getUsers((err, users) => { err ? reject(err) : resolve(users) });
    });
}

exports.getCountriesFromDb = () => {
    return new Promise((resolve, reject) => {
        database.getCountries((err, countries) => { err ? reject(err) : resolve(countries) });
    });
}

exports.getInterestsFromDb = () => {
    return new Promise((resolve, reject) => {
        database.getInterests((err, interests) => { err ? reject(err) : resolve(interests) });
    });
}

exports.getBirthYearsFromDb = () => {
    return new Promise((resolve, reject) => {
        database.getBirthYears((err, years) => { err ? reject(err) : resolve(years) });
    });
}




// --- mutations --- //

exports.createUser = (userList) => {
    return new Promise((resolve, reject) => {
        userList.forEach(u => {
            let newUser = new User(u);
            let uuid = getUuid();
            
            newUser._id = uuid;
            newUser.id  = uuid;

            database.addUser(newUser, (err, res) => {
                if (err) reject(err);
            });
        });

        resolve('Users have succesfully been added');
    });
}

exports.removeUser = (userId) => {
    return new Promise((resolve, reject) => {
        database.removeUser(userId, (err, res) => {
            err ? reject(err) : resolve('User succesfully removed');
        });
    });
}




// --- help methods --- //

const getUuid = () => {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
