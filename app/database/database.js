"use strict"

const mongodb = require("mongodb");
const mongoose = require("mongoose");
const config = require("../../config");

const UserSchema = require("../database_schemas/schema_user");

const User = require("../models/User");


/*
 * In deze file worden enkel db-acties uitgevoerd,
 * data wordt hier niet verwerkt
 */
//module.exports = (() => {

    // --- init --- //

    const DATABASE_URI = config.database.DATABASE_URI;
    const DATABASE_OPTIONS = config.database.DATABASE_OPTIONS;


    (() => {
        mongoose.Promise = global.Promise;
        mongoose.connect(DATABASE_URI, DATABASE_OPTIONS, function(err, db) {
            if (err) {
                console.error("error on connection:", err);
            } else {
                console.info("db connect");
            }
        });
    })();



    
    // --- make connection --- //

    let connect = function(callback) {

        mongoose.connect(DATABASE_URI, DATABASE_OPTIONS, function(err, db) {
            if (err) {
                callback(null);
            } else {
                callback(db);
            }
        });

    }




    // --- Toevoegen aan database --- //

    // User
    exports.addUser = (user, callback) => {
        connect((db) => {

            let u = new UserSchema(user);
            u.save((err) => {
                if (err)
                    callback(err, null);
                else
                    callback(null, u);
            });
        });
    }

    exports.removeUser = (userId, callback) => {
        connect((db) => {
            UserSchema.remove({ id: userId }, (err, res) => {
                err ? callback(err, null) : callback(null, res);
            });
        });
    }




    // --- Ophalen uit database --- //

    // User
    exports.getUsers = (callback) => {
        connect((db) => {

            UserSchema.find({}, (err, users) => {
                if (err) {
                    callback(err, null);
                } else {
                    let list = [];

                    users.forEach(u => {
                        list.push(new User(u));
                    });
                    
                    callback(null, list);
                }
            });

        });
    }

    exports.getBirthYears = (callback) => {
        connect((db) => {

            UserSchema.find({}).distinct('birthday').exec((err, birthdays) => {
                if (err) {
                    callback(err, null);
                } else {
                    let list = [];

                    birthdays.forEach(day => {
                        let year = day.split('-')[0];
                        if (!list.includes(year))
                            list.push(year);
                    });

                    callback(null, list.sort());
                }
            });

        });
    }

    exports.getCountries = (callback) => {
        connect((db) => {

            UserSchema.find({}).distinct('country').exec((err, countries) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, countries.sort());
                }
            });

        });
    }

    exports.getInterests = (callback) => {
        connect((db) => {

            UserSchema.find({}).distinct('interests').exec((err, interests) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, interests.sort());
                }
            });

        });
    }

    exports.getFilteredUsers = (filters, callback) => {
        connect((db) => {

            let lastname = new RegExp("^" + filters.lastname, 'i');
            let country = filters.country;
            let year = new RegExp('^' + filters.birthyear);

            let interests = [];
            if (filters.interests && filters.interests.length > 0)
                filters.interests.forEach(i => { interests.push(i) });

            let findObj  = {};
            if (filters.lastname) findObj.lastname = lastname;
            if (filters.birthyear) findObj.birthday = year;
            if (filters.country) findObj.country = country;
            if (interests.length > 0) findObj.interests = { "$all": interests };
            
            UserSchema.find(findObj, (err, users) => {
                if (err) {
                    console.error(err);
                    callback(err, null);
                } else {
                    let list = [];

                    users.forEach(u => {
                        list.push(new User(u));
                    });
                    
                    callback(null, list);
                }
            });

        });
    }

//})();
