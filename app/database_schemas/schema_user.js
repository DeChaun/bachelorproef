"use strict"

const mongoose = require("mongoose");
const config = require('../../config');

mongoose.Promise = global.Promise;

let userSchema = new mongoose.Schema({
    _id: {
        type: String
    },
    id: {
        type: String,
        unique: true
    },
    firstname: {
        type: String,
        unique: true
    },
    lastname: {
        type: String
    },
    birthday: {
        type: String
    },
    country: {
        type: String
    },
    city: {
        type: String
    },
    address: {
        type: String
    },
    interests: {
        type: [String]
    },
    comment: {
        type: String
    },
    __v: {
        type: Number,
        select: false
    }
});

module.exports = mongoose.model(config.database.collectionNames.COLLECTION_USER, userSchema, config.database.collectionNames.COLLECTION_USER);