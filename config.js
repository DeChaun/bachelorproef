module.exports = {
	port: 3002,
    mainDir: __dirname,
    jwtSecretKey: "mySecretKey",
    database: {
        // DATABASE_URI: "mongodb://localhost:27017/bachelorproef",
		DATABASE_URI: "mongodb://bp:-Azerty123@ds237120.mlab.com:37120/bp",
        DATABASE_OPTIONS: {
            reconnectTries: 5,
        },
        collectionNames: {
            COLLECTION_USER: "user"
        }
    }
}
